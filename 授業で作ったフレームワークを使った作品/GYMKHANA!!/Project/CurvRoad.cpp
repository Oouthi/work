#include <math.h>
#include "CurvRoad.h"
#include "Engine/Model.h"
#include "Engine/BoxCollider.h"
#include "Engine/Direct3D.h"

//素材のパス
const std::string CURV_ROAD_MODEL_PATH = "Data/Model/curvRoad2.fbx";

//コンストラクタ
CurvRoad::CurvRoad(IGameObject * parent)
	:RoadParts(parent, "CurvRoad"), hModel_(-1)
{
}

//デストラクタ
CurvRoad::~CurvRoad()
{
}

//初期化
void CurvRoad::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load(CURV_ROAD_MODEL_PATH);
	assert(hModel_ >= 0);

	//コライダー作成
	//直角二等辺3角形モデルの斜辺にコライダーをつけるのでコライダーの長さが√(モデルのサイズ二乗 + モデルのサイズ二乗)になる
	BoxCollider* pWall = new BoxCollider(CURVE_ROAD_WALL_COLLIDER_NAME, D3DXVECTOR3(WALL_THICKNESS / 4, WALL_HEIGHT / 2, -WALL_THICKNESS / 4),
										 D3DXVECTOR3(sqrt(pow(WALL_THICKNESS, 2) + pow(WALL_THICKNESS, 2)) / 2, WALL_HEIGHT, sqrt(pow(CURVE_ROAD_LENGTH, 2) + pow(CURVE_ROAD_LENGTH, 2))), this);
	pWall->SetRotate(D3DXVECTOR3(0, 45, 0));
	AddCollider(pWall);

	//直角の部分にも細い壁があるのでコライダーをつける
	pWall = new BoxCollider(CURVE_ROAD_RIGHT_ANGLE_WALL_COLLIDER_NAME, D3DXVECTOR3(CURVE_ROAD_LENGTH / 2, WALL_HEIGHT / 2, -CURVE_ROAD_LENGTH / 2),
							D3DXVECTOR3(sqrt(pow(WALL_THICKNESS, 2) + pow(WALL_THICKNESS, 2)), WALL_HEIGHT, sqrt(pow(WALL_THICKNESS, 2) + pow(WALL_THICKNESS, 2))), this);
	pWall->SetRotate(D3DXVECTOR3(0, 45, 0));
	AddCollider(pWall);
}

//更新
void CurvRoad::Update()
{
}

//描画
void CurvRoad::Draw()
{
	//プロジェクション行列
	D3DXMATRIX proj;
	//最後に設定したプロジェクション行列を取ってくる
	Direct3D::pDevice->GetTransform(D3DTS_PROJECTION, &proj);

	//ビュー行列
	D3DXMATRIX view;
	//最後に設定したビュー行列を取ってくる
	Direct3D::pDevice->GetTransform(D3DTS_VIEW, &view);

	//シェーダーに渡すための行列を合成
	//ワールド x ビュー x プロジェクション
	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	//シェーダー側に合成した行列をセット
	//第1引数：HLSLのグローバル変数名
	//第2引数：オブジェクト側から渡す行列
	pEffect_->SetMatrix("WVP", &matWVP);

	//いったんワールド行列をコピー
	D3DXMATRIX mat = worldMatrix_;

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	//面が横に伸びたら法線は縦向きに
	//面が縦に伸びたら法線は横向きにしたい
	//拡大縮小行列は逆行列にしたい
	D3DXMatrixInverse(&scale, nullptr, &scale);

	//回転行列と拡大縮小行列の逆行列を合成してシェーダーに渡す
	//※拡大縮小行列の逆行列をかけたので、このベクトルの長さは1じゃない
	mat = scale * rotateZ * rotateX * rotateY;
	pEffect_->SetMatrix("RS", &mat);

	//ライトの方向を決めてるベクトルをDirect3Dから取ってくる
	D3DLIGHT9 lightState;
	Direct3D::pDevice->GetLight(0, &lightState);
	//ライトの方向を決めてるベクトルを渡す
	//ライト->物体へのベクトル
	//※LambertShaderは物体->ライトのベクトルを使うので反転させる必要あり
	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//ワールド行列を渡す
	pEffect_->SetMatrix("W", &worldMatrix_);

	//BeginからEndまでの範囲にシェーダーを適用する
	pEffect_->Begin(NULL, 0);
	pEffect_->BeginPass(0);

	Model::SetMatrix(hModel_, worldMatrix_);
	Model::SetEffect(hModel_, pEffect_);
	Model::Draw(hModel_);

	pEffect_->EndPass();
	pEffect_->End();
}

//開放
void CurvRoad::Release()
{
}