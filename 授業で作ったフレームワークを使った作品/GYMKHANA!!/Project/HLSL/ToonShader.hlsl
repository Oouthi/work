float4x4 WVP;	//ワールド行列＊ビュー行列＊プロジェクション行列
float4x4 RS;	//回転行列＊拡大縮小行列を受け取る
float4x4 W;		//ワールド行列を受け取る
float4	 LIGHT_DIR;//光の方向ベクトル(受け取った時点では光->物体のベクトル)
float4	 DIFFUSE_COLOR;	//テクスチャ以外の色を受け取る(マテリアルの色)
float4	 AMBIENT_COLOR;	//環境光
float4	 SPECULER_COLOR;//鏡面反射光(ハイライト)
float	 SPECULER_POWER;//鏡面反射光の強さ
float4	 CAMERA_POS;	//カメラの位置
bool	 IS_TEXTURE;	//テクスチャがあるか
texture	 TEXTURE;		//テクスチャ
texture	 TEXTURE_TOON;	//はっきりした陰影をつけるためのテクスチャ

//サンプラー
//テクスチャを表示するときの設定
sampler texSampler = sampler_state
{
	Texture = <TEXTURE>;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = Wrap;
	AddressV = Wrap;
};

sampler toonSampler = sampler_state
{
	Texture = <TEXTURE_TOON>;
	AddressU = Clamp;
	MinFilter = LINEAR;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
};

//頂点シェーダーの戻り値に使う構造体
struct VS_OUT
{
	float4 pos    : SV_POSITION;	
	float4 normal : NORMAL;			
	float4 eye	  : TEXCOORD1;		
	float2 uv	  : TEXCOORD0;		
};

//頂点シェーダー
VS_OUT VS(float4 pos : POSITION, float4 normal : NORMAL, float2 uv : TEXCOORD0)
{
	VS_OUT outData;

	//ローカルポジションにワールド行列＊ビュー行列＊プロジェクション行列をかける
	outData.pos = mul(pos, WVP);			

	//法線に回転行列と拡大行列をかけて正規化
	normal = mul(normal, RS);				
	normal = normalize(normal);			
	outData.normal = normal;

	//視線ベクトルを求める
	float4 worldPos = mul(pos, W);
	outData.eye = normalize(CAMERA_POS - worldPos);

	outData.uv = uv;

	return outData;
}

//ピクセルシェーダー
float4 PS(VS_OUT inData) : COLOR
{
	inData.normal = normalize(inData.normal);
	inData.eye = normalize(inData.eye);

	float4 lightDir = LIGHT_DIR;	
	lightDir = normalize(lightDir);	

	//光源と法線の内積から拡散反射光の強さを求めて陰影用の画像からディフューズの明るさを決める
	float uvX = dot(inData.normal, -lightDir);
	float4 diffuse = tex2D(toonSampler, float2(uvX, 0));
	diffuse.a = 1;

	//テクスチャかマテリアルの色を掛け入れる
	if (IS_TEXTURE == false)
	{
		diffuse *= DIFFUSE_COLOR;
	}
	else
	{
		diffuse *= tex2D(texSampler, inData.uv);
	}

	//アンビエント光
	float4 ambient = AMBIENT_COLOR;

	//鏡面反射光
	//光と法線の反射ベクトルを求める
	float4 R = reflect(lightDir, inData.normal);
	//反射ベクトルと視線ベクトルの内積でハイライトの強さの基準を求めてべき乗することで
	//視線ベクトルと反射ベクトルが近いほど指数的にハイライトが強まる
	float4 speculer = pow(dot(R, inData.eye), SPECULER_POWER) * 2 * SPECULER_COLOR;

	return diffuse + ambient + speculer;
}

//輪郭用頂点シェーダ
float4 VS_Toon(float4 pos : POSITION, float4 normal : NORMAL) : SV_POSITION
{
	//頂点位置を法線方向に少し動かすことで拡大し、輪郭として使う
	normal.w = 0;
	pos += normal / 50;
	pos = mul(pos, WVP);
	return pos;
}

float4 PS_Toon(float4 pos : SV_POSITION) : COLOR
{
	//黒い輪郭にする
	return float4(0,0,0,1);
}

technique
{
	pass
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}

	pass
	{
		VertexShader = compile vs_3_0 VS_Toon();
		PixelShader = compile ps_3_0 PS_Toon();
	}
}