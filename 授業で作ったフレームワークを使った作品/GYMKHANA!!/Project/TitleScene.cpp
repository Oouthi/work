#include "TitleScene.h"
#include "Engine/SceneManager.h"
#include "Engine/Input.h"
#include "Engine/Image.h"

//素材のパス
const std::string TITLE_PICT_PATH = "Data/title.png";

//コンストラクタ
TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "TitleScene"), hTitlePict_(-1)
{
}

//初期化
void TitleScene::Initialize()
{
	hTitlePict_ = Image::Load(TITLE_PICT_PATH);
}

//更新
void TitleScene::Update()
{
	//エンターを押したらプレイシーンに移る
	if (Input::IsKeyDown(DIK_RETURN))
	{
		((SceneManager*)pParent_)->ChangeScene(SCENE_ID_PLAY);
	}
}

//描画
void TitleScene::Draw()
{
	Image::SetMatrix(hTitlePict_, worldMatrix_);
	Image::Draw(hTitlePict_);
}

//開放
void TitleScene::Release()
{
}