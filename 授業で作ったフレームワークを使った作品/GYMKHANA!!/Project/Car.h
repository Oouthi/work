#pragma once
#include "Engine/IGameObject.h"
#include "Engine/SphereCollider.h"
#include "Engine/BoxCollider.h"
#include "Engine/Input.h"

//車を管理するクラス
class Car : public IGameObject
{
	//HLTLで書いた処理がここに入る
	LPD3DXEFFECT pEffect_;

	LPDIRECT3DTEXTURE9 pToonTex_;

	//3Dモデルの番号
	int hModel_;

	//数字画像の画像番号
	int hNumberPict_[10];

	//ギアのRの画像番号
	int hGearRPict_;

	//k/hの文字画像番号
	int hKParHPict_;

	//gearの文字画像番号
	int hGearPict_;

	//RPMの文字画像番号
	int hRPMPict_;
	
	//エンジン回転数
	float RPM_;

	//エンジンの角回転速度
	float engineVel_;

	//エンジントルク
	float engineTorque_;

	//車輪のトルク
	float wheelTorque_;

	//車輪の回転
	float wheelVel_;

	//抵抗トルク
	float resistanceTorque_;

	//一秒に進む速度
	float speed_;

	//舵角
	float tireAngle_;

	//クラッチのつなぎ具合
	float clutch_;

	//ミッション側の回転
	float MTVel_;

	enum GEAR
	{
		GEAR_1 = 0,
		GEAR_2,
		GEAR_3,
		GEAR_4,
		GEAR_5,
		GEAR_6,
		GEAR_R,
		GEAR_MAX
	}gear_;

	//走行状態
	enum MOVE_STATE
	{
		MOVE_FORWARD = 0,
		MOVE_BACK,
		STOP,
		DRIVE_STATE_MAX
	}moveState_;

	//moveStatae_によってよぶ関数を変える
	void(Car::*DriveStateFunc_)();

public:
	//コンストラクタ
	Car(IGameObject* parent);

	//デストラクタ
	~Car();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(IGameObject *pTarget) override;

private:

	//トルク計算
	void TorqueCalc();

	//右左折した時の位置を計算
	void TurningPositionCalc();

	//直進する時の位置計算
	void StraightCalc();

	//ギアチェンジ計算
	void GearChange();

	//アクセルを踏んでいる時のクラッチを繋ぐ処理やエンジンの回転数やトルクの計算をする処理の過程
	void Accele();

	//アクセル踏んでいない時の処理
	void NotAccele();

	//DriveStateFunc_()に入る関数

	//前進している時の処理
	void MoveForward();

	//後退している時の処理
	void MoveBack();

	//止まっている時の処理
	void Stop();
};
