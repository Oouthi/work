#define _USE_MATH_DEFINES
#include <math.h>
#include <map>
#include <string>
#include "Car.h"
#include "Engine/Model.h"
#include "Engine/Direct3D.h"
#include "Engine/Image.h"
#include "Engine/Camera.h"
#include "Engine/SphereCollider.h"
#include "Engine/Direct3D.h"
#include "CurvRoad.h"
#include "StraightRoad.h"
#include "Stage.h"
#include "PlayScene.h"

//フレーム数
const float FPS = 60.0f;

//動摩擦係数
const float DYNAMIC_FRICTION = 0.6f;

//転がり摩擦係数
//転がっている物(タイヤ)が受ける摩擦　普通の動摩擦に比べて小さい
const float ROLLING_FRICTION = 0.025f;

//車の重さkg
const int CAR_WEIGHT = 1500;

//重力加速度 N kg
const float GRAVITY = 9.8f;

//タイヤの大きさm(直径)
const float TIRE_DIAMETER = 0.65f;

//減速比 どのギアでもかかるエンジンとタイヤのギア比
const float REDUCTION_RATIO = 3.7f;

//クラッチの摩擦
const float CLUTCH_FRICTION = 0.6f;

//エンジンの慣性モーメント
const float ENGINE_INERTIA = 3.0f;

//エンジン内の摩擦係数
const float ENGINE_FRICTION = 0.007f;

//アイドリング時のエンジン角回転数
const float IDLING_VEL = 160.0f;

//ブレーキトルク 適当 現実でブレーキがどれくらいの力でかかるかは調査中
const float BRAKE_TORQUE = 8000.0f;

//空気抵抗係数
const float AIR_FRICTION = 0.27f;

//流体密度
const float FLUID_DENSITY = 1.2f;

//前面投影面積
const float FRONT_AREA = 2.1f;

//ホイールベース m
const float WHEELBASE = 2.5f;

//クラッチを完全に繋げる基準となるエンジンとミッション側の回転の差
const float ROTATE_DIFFERE = 100.0f;

//変速比
const float GEAR_RATIO[] =
{
	3.6f, 2.2f, 1.5f, 1.2f, 1.0f, 0.75f, 3.0f
};

//車の走行状態 1が前進 -1がバック 0が停止
const int MOVE_DIR[] =
{
	1, -1, 0
};

//配列番号 + 1のギアを上げる目安
//gearChangeCriteria[0]は1ギアを2ギアに上げる基準
const int GEAR_CHANGE_CRITERIA_ARRAY[] = { 17, 30, 44, 56, 70 };
//ギアを下げる際にgearChangeCriteriaから引く値
//ギアを上げた後アクセルで半クラになる時トルクが半分しかタイヤに与えられないので抵抗に勝てずにスピードが下がる場合があり
//すぐにギア変更の基準値を下回ってしまうのを防ぐため
const int GEAR_DOWN_CRITERIA = 5;

//転がり抵抗
//車の重さ * 重力 * 転がり摩擦係数 * タイヤの直径
const float ROLLING_RESISTANCE_TORQUE = CAR_WEIGHT * GRAVITY * ROLLING_FRICTION * TIRE_DIAMETER;

//スタビリティファクター 
//車の重心位置とニュートラルステアポイント(タイヤのコーナリングパワーのバランスが取れる位置との差)
//この値が0より小さければオーバーステア(速度があるほど旋回半径が小さくなる)、
//		  0より大きければアンダーステア(速度があるほど旋回半径が大きくなる)、
//		  0ならニュートラルステア(速度によって旋回半径は変わらない)
const float STABILITY_FACTOR = 0.0f;

//配列番号かける1000の回転数で発生するトルクが入っている
const int TORQUE[] = { 0, 250, 400, 550, 700, 650, 600, 500, 350, 0 , 0 };

//画像を複数表示する際重ならないようにずらすための値
const int IMAGE_SIZE_X = 40;
const int IMAGE_Y = -80;

//1フレームで変わるハンドルの角度
const float ADD_TIRE_ANGLE = 0.4f;
//最高舵角 deg
const float MAX_TIRE_ANGLE = 4.0f;

//当たり判定をつける際に使う
const float CAR_LENGHT = 4.5f;
const float CAR_HEIGHT = 1.0f;
const float CAR_WIDTH = 2.0f;

//カメラの位置
const D3DXVECTOR3 CAMERA_POS = D3DXVECTOR3(0, 2, -3);
const D3DXVECTOR3 CAMERA_TARGET = D3DXVECTOR3(0, 1, 0);

//素材のパス
const std::string NUMBER_PICT_PATH[] = { "Data/CarNumberPict/zero.png", 
										  "Data/CarNumberPict/one.png", 
										  "Data/CarNumberPict/two.png", 
										  "Data/CarNumberPict/three.png", 
										  "Data/CarNumberPict/four.png", 
										  "Data/CarNumberPict/five.png", 
										  "Data/CarNumberPict/six.png", 
										  "Data/CarNumberPict/seven.png", 
										  "Data/CarNumberPict/eight.png",
										  "Data/CarNumberPict/nine.png", };
const std::string GEAR_R_PICT_PATH = "Data/gearR.png";
const std::string KM_PAR_HOUR_PICT_PATH = "Data/kph.png";
const std::string GEAR_PICT_PATH = "Data/gear.png";
const std::string RPM_PICT_PATH = "Data/RPM.png";
const std::string CAR_MODEL_PATH = "Data/Model/lamborghini.fbx";

//コンストラクタ
Car::Car(IGameObject * parent)
	:IGameObject(parent, "Car"), hModel_(-1), RPM_(0), speed_(0), gear_(GEAR_1), engineVel_(0),
	wheelTorque_(0), resistanceTorque_(0), wheelVel_(0), hKParHPict_(-1), hGearPict_(-1),
	tireAngle_(0), clutch_(0), MTVel_(0), hRPMPict_(-1),moveState_(STOP), hGearRPict_(-1), DriveStateFunc_(nullptr)
{
}

//デストラクタ
Car::~Car()
{
}

//初期化
void Car::Initialize()
{
	//普通のシェーダを使う
	//HLSLで書いたシェーダーをロード
	//第一引数：デバイスオブジェクト
	//第二引数：使いたいシェーダーのファイル名(パス)
	//第七引数：格納先のポインタ
	//エラーを入れる変数
	LPD3DXBUFFER err = 0;
	if (D3DXCreateEffectFromFile(Direct3D::pDevice,
		"HLSL/ToonShader.hlsl", NULL, NULL,
		D3DXSHADER_DEBUG, NULL, &pEffect_, &err))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(),
			"シェーダーエラー", MB_OK);
	}

	assert(pEffect_ != nullptr);

	//影用の画像ファイルを読み込んでテクスチャ作成
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, "Data/Toon.png", 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT,
			D3DX_FILTER_NONE, D3DX_DEFAULT, 0, nullptr, nullptr, &pToonTex_);
	assert(pToonTex_);
	//hlslファイルに渡す
	pEffect_->SetTexture("TEXTURE_TOON", pToonTex_);

	//モデルデータのロード
	hModel_ = Model::Load(CAR_MODEL_PATH);
	assert(hModel_ >= 0);

	//数字画像ロード

	for (int i = 0; i < 10; i++)
	{
		hNumberPict_[i] = Image::Load(NUMBER_PICT_PATH[i]);
		assert(hNumberPict_[i] >= 0);
	}

	//ギアのR画像のロード
	hGearRPict_ = Image::Load(GEAR_R_PICT_PATH);

	//gearの画像ロード
	hGearPict_ = Image::Load(GEAR_PICT_PATH);

	//k/hの画像ロード
	hKParHPict_ = Image::Load(KM_PAR_HOUR_PICT_PATH);

	//RPMの画像ロード
	hRPMPict_ = Image::Load(RPM_PICT_PATH);

	//当たり判定
	BoxCollider* collision = new BoxCollider(D3DXVECTOR3(0, CAR_HEIGHT / 2, 0), D3DXVECTOR3(CAR_WIDTH, CAR_HEIGHT, CAR_LENGHT), this);
	AddCollider(collision);

	//エンジンの回転に強制的に数字を入れる
	//現実でいうエンジンをかけたときの状態
	engineVel_ = IDLING_VEL;

	//初期位置はゴールより手前にしたいので車体一つ分後ろに下げる
	position_.z -= CAR_LENGHT;
	Transform();

	//カメラ
	Camera* pCamera = CreateGameObject<Camera>(this);
	//カメラの位置とカメラが見る位置を設定
	pCamera->SetPosition(CAMERA_POS);
	pCamera->SetTarget(CAMERA_TARGET);

	//初期ステートにStop関数を入れておく
	DriveStateFunc_ = &Car::Stop;
}

//更新
void Car::Update()
{
	//チェックポイントとゴールに触れているかどうかのフラグをリセットする
	((PlayScene*)pParent_)->SetGoalFlag(false);
	((PlayScene*)pParent_)->SetCheckPointFlag(false);

	//ギアチェンジ
	GearChange();

	//ミッション側の角速度
	//(スピード / (タイヤの直径 * 円周率)) * 減速比 * 変速比 * 2 * 円周率
	MTVel_ = (speed_ / (TIRE_DIAMETER * M_PI)) * REDUCTION_RATIO * GEAR_RATIO[gear_] * 2 * M_PI;

	//抵抗計算
	resistanceTorque_ = 0;

	//空気抵抗
	//空気抵抗係数 * 流体密度 * 前面投影面積 * スピード^2 / 2 * タイヤの直径 / 2
	resistanceTorque_ += AIR_FRICTION * FLUID_DENSITY * FRONT_AREA * pow(speed_, 2) / 2 * TIRE_DIAMETER / 2;

	//転がり抵抗
	resistanceTorque_ += ROLLING_RESISTANCE_TORQUE;

	//曲がる時の処理
	//右に曲がる時
	if (Input::IsKey(DIK_RIGHT) && !Input::IsKey(DIK_LEFT))
	{
		if (tireAngle_ < MAX_TIRE_ANGLE)
		{
			tireAngle_ += ADD_TIRE_ANGLE;
		}
	}
	//左に曲がる時
	else if (Input::IsKey(DIK_LEFT) && !Input::IsKey(DIK_RIGHT))
	{
		if (tireAngle_ > -MAX_TIRE_ANGLE)
		{
			tireAngle_ -= ADD_TIRE_ANGLE;
		}
	}
	//曲がるボタンが押されていなければタイヤの角度はまっすぐになっていく
	else
	{
		if (tireAngle_ < 0)
		{
			tireAngle_ += ADD_TIRE_ANGLE;
		}
		else if(tireAngle_ > 0)
		{
			tireAngle_ -= ADD_TIRE_ANGLE;
		}

		if (abs(tireAngle_) <= ADD_TIRE_ANGLE)
		{
			tireAngle_ = 0;
		}
	}
	
	//走行状態によって入る関数が変わる
	(this->*DriveStateFunc_)();
}

//描画
void Car::Draw()
{
	//プロジェクション行列
	D3DXMATRIX proj;
	//最後に設定したプロジェクション行列を取ってくる
	Direct3D::pDevice->GetTransform(D3DTS_PROJECTION, &proj);

	//ビュー行列
	D3DXMATRIX view;
	//最後に設定したビュー行列を取ってくる
	Direct3D::pDevice->GetTransform(D3DTS_VIEW, &view);

	//シェーダーに渡すための行列を合成
	//ワールド x ビュー x プロジェクション
	D3DXMATRIX matWVP = worldMatrix_ * view * proj;

	//シェーダー側に合成した行列をセット
	//第1引数：HLSLのグローバル変数名
	//第2引数：オブジェクト側から渡す行列
	pEffect_->SetMatrix("WVP", &matWVP);

	//いったんワールド行列をコピー
	D3DXMATRIX mat = worldMatrix_;

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, scale_.x, scale_.y, scale_.z);
	//面が横に伸びたら法線は縦向きに
	//面が縦に伸びたら法線は横向きにしたいので
	//拡大縮小行列は逆行列にしたい
	D3DXMatrixInverse(&scale, nullptr, &scale);

	//回転行列と拡大縮小行列の逆行列を合成してシェーダーに渡す
	//※拡大縮小行列の逆行列をかけたので、このベクトルの長さは1じゃない
	mat = scale * rotateZ * rotateX * rotateY;
	pEffect_->SetMatrix("RS", &mat);

	//ライトの方向を決めてるベクトルをDirect3Dから取ってくる
	D3DLIGHT9 lightState;
	Direct3D::pDevice->GetLight(0, &lightState);
	//ライトの方向を決めてるベクトルを渡す
	//ライト->物体へのベクトル
	//※LambertShaderは物体->ライトのベクトルを使うので反転させる必要あり
	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//カメラの位置をシェーダーに渡す
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&(CAMERA_POS + position_));

	//ワールド行列を渡す
	pEffect_->SetMatrix("W", &worldMatrix_);

	Model::SetMatrix(hModel_, worldMatrix_);
	Model::SetEffect(hModel_, pEffect_);

	//BeginからEndまでの範囲にシェーダーを適用する
	pEffect_->Begin(NULL, 0);

	//普通に表示
	pEffect_->BeginPass(0);
	Model::Draw(hModel_);
	pEffect_->EndPass();

	//輪郭用シェーダ
	pEffect_->BeginPass(1);
	//カリングモードを逆にしてカメラの方を向いている面を非表示にすることによって
	//少し拡大された黒いモデルが輪郭として表示できる
	Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
	Model::Draw(hModel_);
	Direct3D::pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	pEffect_->EndPass();

	pEffect_->End();

	//画像表示
	//画像を表示する位置に使う変数
	D3DXMATRIX m;
	D3DXMatrixIdentity(&m);

	enum DIGIT
	{
		ONE = 0,
		TEN,
		HUNDRED,
		THOUSAND,
		DIGIT_PLACE_MAX
	};

	//n桁目の数字を入れる配列
	short int digitArray[DIGIT_PLACE_MAX];

	//画像の位置変数
	//2桁以上表示しなければいけない数値をfor文の中でずらしていくのに必要
	//スピードとエンジン回転数の表示で使われる
	float imageX;

	//スピード表示
	//スピードがm/sなのでkm/hに直す
	float hourlySpeed = speed_ * 60 * 60 / 1000;
	
	imageX = 140;
	//100の位から表示する
	for (int i = HUNDRED; i >= ONE; i--)
	{
		digitArray[i] = hourlySpeed / pow(10, i);
		hourlySpeed -= digitArray[i] * pow(10, i);

		imageX += IMAGE_SIZE_X;

		D3DXMatrixTranslation(&m, imageX, IMAGE_Y, 0);
		Image::SetMatrix(hNumberPict_[digitArray[i]], m);
		Image::Draw(hNumberPict_[digitArray[i]]);
	}

	//km/h表示
	imageX += 70;
	D3DXMatrixTranslation(&m, imageX, IMAGE_Y, 0);
	Image::SetMatrix(hKParHPict_, m);
	Image::Draw(hKParHPict_);

	//ギア表示
	imageX += 100;
	D3DXMatrixTranslation(&m, imageX, IMAGE_Y, 0);
	if (!(gear_ == GEAR_R))
	{
		Image::SetMatrix(hNumberPict_[gear_ + 1], m);
		Image::Draw(hNumberPict_[gear_ + 1]);
	}
	else
	{
		Image::SetMatrix(hGearRPict_, m);
		Image::Draw(hGearRPict_);
	}

	//gear文字表示
	imageX += 60;
	D3DXMatrixTranslation(&m, imageX, IMAGE_Y, 0);
	Image::SetMatrix(hGearPict_, m);
	Image::Draw(hGearPict_);

	//回転数の表示
	int RPM = engineVel_ * 60 / (2.0 * M_PI);
	imageX += 40;
	
	//1000の位から表示する
	for (int i = THOUSAND; i >= ONE; i--)
	{
		digitArray[i] = RPM / pow(10, i);
		RPM -= digitArray[i] * pow(10, i);

		imageX += IMAGE_SIZE_X;

		D3DXMatrixTranslation(&m, imageX, IMAGE_Y, 0);
		Image::SetMatrix(hNumberPict_[digitArray[i]], m);
		Image::Draw(hNumberPict_[digitArray[i]]);
	}

	//RPM文字表示
	imageX += 60;
	D3DXMatrixTranslation(&m, imageX, IMAGE_Y, 0);
	Image::SetMatrix(hRPMPict_, m);
	Image::Draw(hRPMPict_);
}

//開放
void Car::Release()
{
	SAFE_RELEASE(pEffect_);
	SAFE_RELEASE(pToonTex_);
}

//何かに当たった
void Car::OnCollision(IGameObject * pTarget)
{
	//壁の位置情報
	struct WALL_INFO
	{
		D3DXVECTOR3 localPosition;
		D3DXVECTOR3 normal;
	};

	//コライダー名と壁の情報を結びつけるためのmap
	std::map<std::string, WALL_INFO> wallInfoMap;
	
	//コライダー名と壁の情報をを結びつける
	//まっすぐな道の右側の壁の位置情報
	wallInfoMap[STRIGHT_ROAD_RIGHT_WALL_COLLIDER_NAME] =
	{ D3DXVECTOR3(STRAIGHT_ROAD_WIGHT / 2 - WALL_THICKNESS, 0, 0), D3DXVECTOR3(-1, 0, 0)};
	//まっすぐな道の左側の壁の位置情報
	wallInfoMap[STRIGHT_ROAD_LEFT_WALL_COLLIDER_NAME] =
	{ D3DXVECTOR3(-(STRAIGHT_ROAD_WIGHT / 2 - WALL_THICKNESS), 0, 0), D3DXVECTOR3(1, 0, 0)};
	//曲がる道の壁の位置情報
	wallInfoMap[CURVE_ROAD_WALL_COLLIDER_NAME] =
	{ D3DXVECTOR3(WALL_THICKNESS / 2, 0, -WALL_THICKNESS / 2), D3DXVECTOR3(sqrtl(2)/ 2, 0, -sqrtl(2) / 2)};
	//曲がる道の直角部分にある壁の位置情報
	wallInfoMap[CURVE_ROAD_RIGHT_ANGLE_WALL_COLLIDER_NAME] =
	{ D3DXVECTOR3((CURVE_ROAD_LENGTH / 2) - (WALL_THICKNESS / 2), 0, -(CURVE_ROAD_LENGTH / 2) + (WALL_THICKNESS / 2)), D3DXVECTOR3(-sqrtl(2) / 2, 0, sqrtl(2) / 2) };

	//当たったコライダーを調べる
	for (auto itr = colliderList_.begin(); itr != colliderList_.end(); itr++)
	{
		//当たった壁のワールド座標
		D3DXVECTOR3 wallWorldPosition;
		//当たった壁のローカル座標
		D3DXVECTOR3 wallLocalPosition;
		//当たった壁の法線
		D3DXVECTOR3 wallNormal;
		//y軸回転用の変数
		D3DXMATRIX wallRotateMatrixY;
		//ゴールとチェックポイント以外に当たった(壁にめり込んでいた)時は車の位置を戻す
		if ((*itr)->GetHitCollider()->GetName() != GOAL_COLLIDER_NAME && (*itr)->GetHitCollider()->GetName() != CHECKPOINT_COLLIDER_NAME)
		{
			//当たったコライダーの名前から壁の位置と法線を計算する
			//当たった壁がどれだけ回転して配置されているか
			D3DXMatrixRotationY(&wallRotateMatrixY, D3DXToRadian((*itr)->GetHitCollider()->GetGameObject()->GetRotate().y));
			
			//当たった壁がどの位置にあるか
			wallWorldPosition = (*itr)->GetHitCollider()->GetGameObject()->GetPosition();
			
			//当たったのはモデルの原点から見てどの位置にある壁か
			wallLocalPosition = wallInfoMap[(*itr)->GetHitCollider()->GetName()].localPosition;
			D3DXVec3TransformCoord(&wallLocalPosition, &wallLocalPosition, &wallRotateMatrixY);
			
			//ワールド座標とローカル座標を足す
			wallWorldPosition += wallLocalPosition;
			
			//壁の法線をwallInfoMapとwallRotateMatrixY()から計算する
			wallNormal = wallInfoMap[(*itr)->GetHitCollider()->GetName()].normal;
			D3DXVec3TransformCoord(&wallNormal, &wallNormal, &wallRotateMatrixY);

			//ぶつかっている分を押し戻す
			//押し戻した分からスピードも減らす
			D3DXVECTOR3 pushBackVec = position_;
			Collider::PushBackObbFromWall((BoxCollider*)(*itr), wallWorldPosition, wallNormal);
			pushBackVec -= position_;

			//壁にめり込んでいた距離 * 動摩擦 * FPS分スピードを落とす
			//1フレームに戻される距離なので秒速に直すためにFPSをかける
			float decelerationValue = D3DXVec3Length(&pushBackVec)* DYNAMIC_FRICTION * FPS;

			speed_ -= decelerationValue;
			if (speed_ < 0)
			{
				speed_ = 0;
			}
		}

		//ゴールにあたったとき
		//当たったとき車がどっちを向いていたかでゴール出来るかどうかのフラグが変わる
		int rotateY = abs((int)rotate_.y) % 360;
		if((*itr)->GetHitCollider()->GetName() == GOAL_COLLIDER_NAME)
		{
			//ゴールに正面から入ったかどうか
			if ((moveState_ == MOVE_FORWARD && (rotateY < 90  || rotateY > 270)) ||
				(moveState_ == MOVE_BACK && (rotateY > 90 && rotateY < 270)))
			{
				((PlayScene*)pParent_)->SetGoalFlag(true);
			}
			else
			{
				((PlayScene*)pParent_)->SetGoalFlag(false);
				((PlayScene*)pParent_)->SetCheckpointPassingFlag(false);
			}
		}

		//チェックポイントにあたっていた時
		if ((*itr)->GetHitCollider()->GetName() == CHECKPOINT_COLLIDER_NAME)
		{
			//チェックポイントに正面から入ったかどうか
			if ((moveState_ == MOVE_FORWARD && (rotateY <= 90 || rotateY >= 270)) ||
				(moveState_ == MOVE_BACK && (rotateY >= 90 && rotateY <= 270)))
			{
				((PlayScene*)pParent_)->SetCheckPointFlag(true);
			}
			else
			{
				((PlayScene*)pParent_)->SetCheckPointFlag(false);
				((PlayScene*)pParent_)->SetCheckpointPassingFlag(false);
			}
		}
	}	
}

void Car::TorqueCalc()
{
	//回転数の1000の位
	int RPMThousandPlace = RPM_ / 1000;
	//トルク = エンジン回転数の千の位で出るトルク + 
	//(エンジン回転数の千の位 + 1で出るトルク - エンジン回転数の千の位で出るトルク) * 
	//((エンジン回転数 - (エンジン回転数の千の位 * 1000)) / 1000)
	engineTorque_ = TORQUE[RPMThousandPlace] +
					(TORQUE[RPMThousandPlace + 1] - TORQUE[RPMThousandPlace]) *
					((RPM_ - (RPMThousandPlace * 1000)) / 1000);
}

void Car::TurningPositionCalc()
{
	//旋回半径計算
	//1 + スタビリティファクタ * スピード^2 * ホイールベース / タイヤの角度
	float turningRadius = (1 + (STABILITY_FACTOR * pow(speed_, 2))) * WHEELBASE / sin(D3DXToRadian(tireAngle_));

	//車の角度計算
	//(180 * スピード) / (円周率 * 旋回半径) / FPS
	float turningAngle = (180 * speed_) / (M_PI * turningRadius) / FPS;

	//位置計算
	D3DXVECTOR3 rotateMove = D3DXVECTOR3(0, 0, 0);

	rotateMove.x = turningRadius - turningRadius * cos(D3DXToRadian(turningAngle));
	//z軸は右回りも左回りも同じなのでabsで絶対値を出している
	rotateMove.z = abs(turningRadius) * sin(D3DXToRadian(abs(turningAngle))) * MOVE_DIR[moveState_];

	//y軸回転用の変数
	D3DXMATRIX mY;
	D3DXMatrixRotationY(&mY, D3DXToRadian(rotate_.y));

	D3DXVec3TransformCoord(&rotateMove, &rotateMove, &mY);

	//前後どちらに進むかによって回転方向が変わる
	rotate_.y += turningAngle * MOVE_DIR[moveState_];
	position_ += rotateMove;
}

void Car::StraightCalc()
{
	//車が向いている方向の単位ベクトルにspeed_を掛ける
	D3DXVECTOR3 move = D3DXVECTOR3(0, 0, speed_);
	//y軸回転用の変数
	D3DXMATRIX mY;
	D3DXMatrixRotationY(&mY, D3DXToRadian(rotate_.y));

	D3DXVec3TransformCoord(&move, &move, &mY);

	//move(車が一秒間に進む距離と向いている方向) * 前か後ろどちらに進むか / 位置フレームに直すためのゲームのFPS
	position_ += move * MOVE_DIR[moveState_] / FPS;
}


void Car::GearChange()
{
	//バックギアに入っている場合スピードによってギアは変えない
	if (gear_ != GEAR_R)
	{
		//スピードがギアの基準値より高ければ上げる
		//6ギアの場合それ以上は上がらない
		if (speed_ > GEAR_CHANGE_CRITERIA_ARRAY[gear_] && gear_ != GEAR_6)
		{
			//ギアをチェンジする前にクラッチを切る
			clutch_ = 0;
			gear_ = (GEAR)(gear_ + 1);
		}
		//スピードがギアの基準値より低ければ下げる
		//1ギアの場合それ以上下がらない
		else if (gear_ != GEAR_1 && speed_ < GEAR_CHANGE_CRITERIA_ARRAY[gear_ - 1] - GEAR_DOWN_CRITERIA)
		{
			//ギアをチェンジする前にクラッチを切る
			clutch_ = 0;
			gear_ = (GEAR)(gear_ - 1);
		}
	}
}

void Car::Accele()
{
	//エンジンの角速度とミッション側の角速度を比べて近い値になったらクラッチを完全につなぐ
	if (clutch_ != 0 && engineVel_ - MTVel_ < ROTATE_DIFFERE && engineVel_ - MTVel_ > 0)
	{
		clutch_ = 1;
	}

	//クラッチを完全につないでいるときはエンジンの角速度がミッションの角速度と同じになる
	if (clutch_ == 1)
	{
		engineVel_ = MTVel_;
	}

	RPM_ = engineVel_ * 60 / (2.0 * M_PI);

	if (clutch_ < 1)
	{
		//クラッチが完全につながっていないときは
		//エンジン自体のトルクによって回転数が上がっていくが
		//クラッチをつなげている分回転が下がる
		if (engineVel_ >= MTVel_)
		{
			//半クラにする
			clutch_ = 0.5;
			//エンジン角回転速度 += ((エンジントルク / エンジンイナーシャ) - ( エンジン角回転速度 * クラッチ)) / FPS
			engineVel_ += ((engineTorque_ / ENGINE_INERTIA) - (engineVel_ * clutch_)) / FPS;
		}
		//エンジンの回転がミッション側の回転より遅い場合(シフトダウンしたときなど)エンジンの回転数が上がるか
		//ミッション側の回転数が下がるまでクラッチをつながない
		else
		{
			clutch_ = 0;
			//エンジン回転数 += (エンジントルク / エンジンイナーシャ) / FPS
			engineVel_ += (engineTorque_ / ENGINE_INERTIA) / FPS;
		}
	}

	TorqueCalc();
}

void Car::NotAccele()
{
	//クラッチが完全に繋がっていない時エンジン内の抵抗とクラッチをつないでいる分角回転速度が下がっていく
	//更にエンジンに燃料供給がなくなるためトルクが0になる
	if (clutch_ == 1)
	{
		engineVel_ = MTVel_;
	}
	else
	{
		//エンジンブレーキトルク = エンジン回転数 * エンジン内の摩擦係数
		float engineBrakeTorque = RPM_ * ENGINE_FRICTION;

		//エンジン回転数 -= エンジンブレーキトルク / エンジンイナーシャ) + (エンジン回転数 * クラッチ)) / FPS
		engineVel_ -= ((engineBrakeTorque / ENGINE_INERTIA) + (engineVel_ * clutch_)) / FPS;
	}

	//アイドリング状態を保つために回転数がエンジンの回転が下がってきたらクラッチを切る
	if (engineVel_ < IDLING_VEL)
	{
		clutch_ = 0;
		engineVel_ = IDLING_VEL;
	}

	engineTorque_ = 0;
}

void Car::MoveForward()
{
	//前進ボタンが押されているか
	if (Input::IsKey(DIK_UP))
	{
		Accele();
	}
	else
	{
		NotAccele();
	}

	//バックボタンが押されていた場合ブレーキとみなす
	if (Input::IsKey(DIK_F) || Input::IsKey(DIK_DOWN))
	{
		resistanceTorque_ += BRAKE_TORQUE;
	}

	//一秒間に進む距離(m)
	//(エンジンのトルク * クラッチのつなぎ具合 * ギア - 抵抗トルク) / タイヤの半径 /　車の重量 / フレーム
	speed_ += (engineTorque_ * clutch_ * REDUCTION_RATIO * GEAR_RATIO[gear_] - resistanceTorque_) / (TIRE_DIAMETER / 2) / CAR_WEIGHT / FPS;

	//スピードが0になれば抵抗も消えるのでスピードが0以下になったらそれ以上抵抗で下がらないようにする
	if (speed_ < 0)
	{
		speed_ = 0;
		moveState_ = STOP;
		DriveStateFunc_ = &Car::Stop;
	}

	//直進するとき
	if (tireAngle_ == 0)
	{
		StraightCalc();
	}
	//曲がるとき
	else
	{
		TurningPositionCalc();
	}
}

void Car::MoveBack()
{
	//後退ボタンが押されているか
	if (Input::IsKey(DIK_DOWN))
	{
		Accele();
	}
	else
	{
		NotAccele();
	}

	//前進ボタンが押されていた場合ブレーキとみなす
	if (Input::IsKey(DIK_F) || Input::IsKey(DIK_UP))
	{
		resistanceTorque_ += BRAKE_TORQUE;
	}

	//一秒間に進む距離(m)
	//(エンジンのトルク * クラッチのつなぎ具合 * ギア - 抵抗トルク) / タイヤの半径 /　車の重量 / フレーム
	speed_ += (engineTorque_ * clutch_ * REDUCTION_RATIO * GEAR_RATIO[gear_] - resistanceTorque_) / (TIRE_DIAMETER / 2) / CAR_WEIGHT / FPS;

	//スピードが0になれば抵抗も消えるのでスピードが0以下になったらそれ以上抵抗で下がらないようにする
	if (speed_ < 0)
	{
		speed_ = 0;
		moveState_ = STOP;
		DriveStateFunc_ = &Car::Stop;
	}

	//直進するとき
	if (tireAngle_ == 0)
	{
		StraightCalc();
	}
	//曲がるとき
	else
	{
		TurningPositionCalc();
	}
}

void Car::Stop()
{
	//アクセルを踏んでいないとき
	NotAccele();

	//前進か後退するボタンが押されたらmoveState_とDriveStateFunc_に入れる関数を変える
	if (Input::IsKey(DIK_UP))
	{
		gear_ = GEAR_1;
		moveState_ = MOVE_FORWARD;
		DriveStateFunc_ = &Car::MoveForward;
		(this->*DriveStateFunc_)();
	}
	else if(Input::IsKey(DIK_DOWN))
	{
		gear_ = GEAR_R;
		moveState_ = MOVE_BACK;
		DriveStateFunc_ = &Car::MoveBack;
		(this->*DriveStateFunc_)();
	}

}
