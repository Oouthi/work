#pragma once
#include "Engine/IGameObject.h"
#include "RoadParts.h"

const float CURVE_ROAD_LENGTH = 17.f;

//曲がる道の壁の名前
const std::string CURVE_ROAD_WALL_COLLIDER_NAME = "CurvRoadWall";
//曲がる道の直角部分にある壁の名前
const std::string CURVE_ROAD_RIGHT_ANGLE_WALL_COLLIDER_NAME = "CurvRoadRightAngleWall";


//直線の道路を管理するクラス
class CurvRoad: public RoadParts
{
	int hModel_;    //モデル番号

public:
	//コンストラクタ
	CurvRoad(IGameObject* parent);

	//デストラクタ
	~CurvRoad();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};