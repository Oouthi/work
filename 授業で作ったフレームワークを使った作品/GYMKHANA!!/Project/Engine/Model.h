#pragma once
#include <string>
#include "Fbx.h"

namespace Model
{
	struct ModelData
	{
		std::string fileName;
		Fbx* pFbx;
		D3DXMATRIX matrix;

		ModelData() : fileName(""), pFbx(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	int Load(std::string fileName, LPD3DXEFFECT pEffect = nullptr);
	void Draw(int handle);
	void SetMatrix(int handle, D3DXMATRIX& matirx);
	void Release(int handle);
	void AllRelease();

	void SetEffect(int handle, LPD3DXEFFECT pEffect);
}