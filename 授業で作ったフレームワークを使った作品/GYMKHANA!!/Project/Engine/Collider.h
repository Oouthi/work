#pragma once

#include <d3dx9.h>
#include <string>

class IGameObject;

//あたり判定のタイプ
enum ColliderType
{
	COLLIDER_BOX,		//箱型
	COLLIDER_CIRCLE		//球体
};


//あたり判定を管理するクラス
class Collider
{
	friend class BoxCollider;
	friend class SphereCollider;

protected:
	IGameObject*	pGameObject_;	//この判定をつけたゲームオブジェクト
	ColliderType	type_;			//種類
	D3DXVECTOR3		rocalCentor_;	//中心位置（ゲームオブジェクトの原点から見た位置）
	D3DXVECTOR3		worldCenter_;	//世界の原点から見た位置
	D3DXVECTOR3		loaclRotate_;	//コライダーの回転
	D3DXVECTOR3		worldRotate_;	//オブジェクトの回転とコライダーの回転を足したもの

	LPD3DXMESH		pMesh_;			//テスト表示用の枠

	Collider* pHitCollider_;		//当たったコライダー

	std::string name_;				//コライダー名

public:
	//コンストラクタ
	Collider(const std::string& name, IGameObject* pGameObject);
	Collider(IGameObject* pGameObject);

	//デストラクタ
	virtual ~Collider();

	//接触判定（継承先のSphereColliderかBoxColliderでオーバーライド）
	//引数：target	相手の当たり判定
	//戻値：接触してればtrue
	virtual bool IsHit(Collider* target) = 0;

	//親の回転情報と位置情報をセットする
	virtual void WorldRotateAndPosCalc() = 0;

	//箱型同士の衝突判定
	//引数：boxA	１つ目の箱型判定
	//引数：boxB	２つ目の箱型判定
	//戻値：接触していればtrue
	bool IsHitBoxVsBox(BoxCollider* boxA, BoxCollider* boxB);

	//箱型と球体の衝突判定
	//引数：box	箱型判定
	//引数：sphere	２つ目の箱型判定
	//戻値：接触していればtrue
	bool IsHitBoxVsCircle(BoxCollider* box, SphereCollider* sphere);

	//球体同士の衝突判定
	//引数：circleA	１つ目の球体判定
	//引数：circleB	２つ目の球体判定
	//戻値：接触していればtrue
	bool IsHitCircleVsCircle(SphereCollider* circleA, SphereCollider* circleB);

	//obbが壁と当たった時戻す
	//当たった壁の法線と位置がわかっている必要がある
	static void PushBackObbFromWall(BoxCollider* obb, D3DXVECTOR3 wallPos, D3DXVECTOR3 wollNormal);

	//ある法線に対する投影線分長を返す
	float ProjectedLineSegment(D3DXVECTOR3 normal, D3DXVECTOR3 dirX, D3DXVECTOR3 dirY, D3DXVECTOR3 dirZ);

	//テスト表示用の枠を描画
	//引数：position	位置
	void Draw();

	void SetGameObject(IGameObject* gameObject) 
	{ 
		pGameObject_ = gameObject; 
	}

	void SetCenter(D3DXVECTOR3 center)
	{
		worldCenter_ = center;
	}

	D3DXVECTOR3 GetWoaldPos()
	{
		return worldCenter_;
	}

	void SetHitCollider(Collider* hitCollider)
	{
		pHitCollider_ = hitCollider;
	}

	Collider* GetHitCollider()
	{
		return pHitCollider_;
	}

	void InisializeHitCollider()
	{
		pHitCollider_ = nullptr;
	}

	IGameObject* GetGameObject()
	{
		return pGameObject_;
	}

	std::string GetName()
	{
		return name_;
	}
};

