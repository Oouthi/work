#include "SphereCollider.h"
#include "BoxCollider.h"
#include "Direct3D.h"


//コンストラクタ（当たり判定の作成）
//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
//引数：size	当たり判定のサイズ
SphereCollider::SphereCollider(const std::string& name, D3DXVECTOR3 center, float diameter, IGameObject* pGameObject) : Collider(name, pGameObject)
{
	rocalCentor_ = center;
	WorldRotateAndPosCalc();
	radius_ = diameter / 2;
	type_ = COLLIDER_CIRCLE;

	//リリース時は判定枠は表示しない
#ifdef _DEBUG
	//テスト表示用判定枠
	D3DXCreateSphere(Direct3D::pDevice, diameter / 2, 8, 4, &pMesh_, 0);
#endif
}

SphereCollider::SphereCollider(D3DXVECTOR3 center, float radius, IGameObject* pGameObject) : SphereCollider("", center, radius, pGameObject)
{
}



void SphereCollider::WorldRotateAndPosCalc()
{
	//オブジェクトが回転したらコライダーのセンターも回転しなければいけない
	D3DXMATRIX mX, mY, mZ;
	D3DXMatrixRotationX(&mX, D3DXToRadian(pGameObject_->GetRotate().x));   //X軸で45°回転させる行列
	D3DXMatrixRotationY(&mY, D3DXToRadian(pGameObject_->GetRotate().y));   //Y軸で90°回転させる行列
	D3DXMatrixRotationZ(&mZ, D3DXToRadian(pGameObject_->GetRotate().z));   //Z軸で30°回転させる行列

	D3DXMATRIX mat;
	D3DXMatrixIdentity(&mat);
	mat = mX * mY * mZ;

	D3DXVec3TransformCoord(&worldCenter_, &rocalCentor_, &mat);

	worldCenter_ += pGameObject_->GetPosition();

	worldRotate_ = loaclRotate_ + pGameObject_->GetRotate();
}

//接触判定
//引数：target	相手の当たり判定
//戻値：接触してればtrue
bool SphereCollider::IsHit(Collider* target)
{
	if (target->type_ == COLLIDER_BOX)
		return IsHitBoxVsCircle((BoxCollider*)target, this);
	else
		return IsHitCircleVsCircle((SphereCollider*)target, this);
}