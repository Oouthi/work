#include "BoxCollider.h"
#include "SphereCollider.h"
#include "IgameObject.h"
#include "Direct3D.h"


//コンストラクタ
Collider::Collider(const std::string& name, IGameObject* pGameObject):
	pGameObject_(nullptr),pMesh_(nullptr)
	, loaclRotate_(D3DXVECTOR3(0,0,0))
{
	name_ = name;
	pGameObject_ = pGameObject;
}

Collider::Collider(IGameObject* pGameObject) : Collider("", pGameObject)
{
}

//デストラクタ
Collider::~Collider()
{
	if (pMesh_ != nullptr)
	{
		pMesh_->Release();
	}
}

//箱型同士の衝突判定
//引数：boxA	１つ目の箱型判定
//引数：boxB	２つ目の箱型判定
//戻値：接触していればtrue
bool Collider::IsHitBoxVsBox(BoxCollider* obb1, BoxCollider* obb2)
{
	//授業で習ったあたり判定
	//D3DXVECTOR3 boxPosA = boxA->pGameObject_->GetPosition() + boxA->center_;
	//D3DXVECTOR3 boxPosB = boxB->pGameObject_->GetPosition() + boxB->center_;

	//if ((boxPosA.x + boxA->size_.x / 2) > (boxPosB.x - boxB->size_.x / 2) &&
	//	(boxPosA.x - boxA->size_.x / 2) < (boxPosB.x + boxB->size_.x / 2) &&
	//	(boxPosA.y + boxA->size_.y / 2) > (boxPosB.y - boxB->size_.y / 2) &&
	//	(boxPosA.y - boxA->size_.y / 2) < (boxPosB.y + boxB->size_.y / 2) &&
	//	(boxPosA.z + boxA->size_.z / 2) > (boxPosB.z - boxB->size_.z / 2) &&
	//	(boxPosA.z - boxA->size_.z / 2) < (boxPosB.z + boxB->size_.z / 2))
	//{
	//	return true;
	//}
	//return false;

	//obbの当たり判定をするにはまず二つのobbの法線ベクトルx,y,zから見たもう一つのobbの投影線分長と
	//二つのobbの法線ベクトルの外積からみた見たobbの投影線分長を求める必要がある
	//投影線分長を求めるには法線ベクトルとobbのx,y,z法線ベクトル * 法線方向の中心から辺までの長さの内積をそれぞれ足すと求められる
	//次に法線ベクトルから見た二つのobbの中心間の距離を求める
	//これは法線ベクトルとobb1の中心位置 - obb2の中心位置のベクトルとの内積で求められる
	//これまでに求められたすべての投影線分長と法線ベクトルから見た二つのobbの中心間の距離を比べ
	//すべて投影線分長のほうが長ければぶつかっていることになる

	//二つのobbの中心間のベクトル
	D3DXVECTOR3 interval = (obb1->GetWoaldPos()) - (obb2->GetWoaldPos());
	//法線ベクトルから見た二つのobbの中心間の距離
	float distanceBetweenCenters;
	//一つ目のobbの投影線分長
	float obb1ProjlineLen;
	//二つ目のobbの投影線分長
	float obb2ProjlineLen;

	//obbの法線ベクトルx, y, zから見たもう一つのobbの投影線分長とobbの中心間の距離を比べてぶつかってるか判定
	for (int dir = DIR_X; dir < DIR_QUANTITY; dir++)
	{
		//obb一個目の法線
		obb1ProjlineLen = obb1->GetLen(dir);

		obb2ProjlineLen = ProjectedLineSegment(obb1->GetDirect(dir),
			obb2->GetDirect(DIR_X) * obb2->GetLen(DIR_X),
			obb2->GetDirect(DIR_Y) * obb2->GetLen(DIR_Y),
			obb2->GetDirect(DIR_Z) * obb2->GetLen(DIR_Z));

		distanceBetweenCenters = fabs(D3DXVec3Dot(&interval, &obb1->GetDirect(dir)));

		if (distanceBetweenCenters > obb1ProjlineLen + obb2ProjlineLen)
		{
			return false; // 衝突していない
		}

		//obb二個目の法線
		obb2ProjlineLen = obb2->GetLen(dir);

		obb1ProjlineLen = ProjectedLineSegment(obb2->GetDirect(dir),
			obb1->GetDirect(DIR_X) * obb1->GetLen(DIR_X),
			obb1->GetDirect(DIR_Y) * obb1->GetLen(DIR_Y),
			obb1->GetDirect(DIR_Z) * obb1->GetLen(DIR_Z));

		distanceBetweenCenters = fabs(D3DXVec3Dot(&interval, &obb2->GetDirect(dir)));

		if (distanceBetweenCenters > obb1ProjlineLen + obb2ProjlineLen)
		{
			return false; // 衝突していない
		}
	}

	//二つのobbの法線ベクトルの外積からみた見たobbの投影線分長とbbの中心間の距離を比べてぶつかってるか判定
	for (int obb1Dir = DIR_X; obb1Dir < DIR_QUANTITY; obb1Dir++)
	{
		for (int obb2Dir = DIR_X; obb2Dir < DIR_QUANTITY; obb2Dir++)
		{
			D3DXVECTOR3 cross;
			D3DXVec3Cross(&cross, &obb1->GetDirect(obb1Dir), &obb2->GetDirect(obb2Dir));

			obb1ProjlineLen = ProjectedLineSegment(cross,
				obb1->GetDirect(DIR_X) * obb1->GetLen(DIR_X),
				obb1->GetDirect(DIR_Y) * obb1->GetLen(DIR_Y),
				obb1->GetDirect(DIR_Z) * obb1->GetLen(DIR_Z));

			obb2ProjlineLen = ProjectedLineSegment(cross,
				obb2->GetDirect(DIR_X) * obb2->GetLen(DIR_X),
				obb2->GetDirect(DIR_Y) * obb2->GetLen(DIR_Y),
				obb2->GetDirect(DIR_Z) * obb2->GetLen(DIR_Z));

			distanceBetweenCenters = fabs(D3DXVec3Dot(&interval, &cross));

			if (distanceBetweenCenters > obb1ProjlineLen + obb2ProjlineLen)
			{
				return false; // 衝突していない
			}
		}
	}

	return true;
}

float Collider::ProjectedLineSegment(D3DXVECTOR3 normal, D3DXVECTOR3 dirX, D3DXVECTOR3 dirY, D3DXVECTOR3 dirZ)
{
	// 3つの内積の絶対値の和で投影線分長を計算
	float projectedLineLength = 0;
	projectedLineLength += fabs(D3DXVec3Dot(&normal, &dirX));
	projectedLineLength += fabs(D3DXVec3Dot(&normal, &dirY));
	projectedLineLength += fabs(D3DXVec3Dot(&normal, &dirZ));
	return projectedLineLength;
}

//箱型と球体の衝突判定
//引数：box	箱型判定
//引数：sphere	２つ目の箱型判定
//戻値：接触していればtrue
bool Collider::IsHitBoxVsCircle(BoxCollider* obb, SphereCollider* sphere)
{
	//授業で習ったあたり判定
	//D3DXVECTOR3 circlePos = sphere->pGameObject_->GetPosition() + sphere->center_;
	//D3DXVECTOR3 boxPos = box->pGameObject_->GetPosition() + box->center_;


	//if (circlePos.x > boxPos.x - box->size_.x - sphere->radius_ &&
	//	circlePos.x < boxPos.x + box->size_.x + sphere->radius_ &&
	//	circlePos.y < boxPos.y + box->size_.y + sphere->radius_ &&
	//	circlePos.y > boxPos.y - box->size_.y - sphere->radius_ &&
	//	circlePos.z > boxPos.z - box->size_.z - sphere->radius_ &&
	//	circlePos.z < boxPos.z + box->size_.z + sphere->radius_ )
	//{
	//	return true;
	//}

	//return false;

	//obbと球との最短距離を入れる変数
	D3DXVECTOR3 shortestDistanceVec(0, 0, 0);   

	// 各軸についてはみ出た部分のベクトルを算出
	for(int dir = DIR_X; dir < DIR_QUANTITY; dir++)
	{
		float len = obb->GetLen(dir);
		//大きさが0のときは計算できない
		if (len <= 0)
		{
			continue;
		}

		//obbの法線ベクトルから見た球の中心までの距離とobbの中心から法線が指している辺までの長さ比率を求める
		float projectionLineRatio = D3DXVec3Dot(&((sphere->GetWoaldPos()) - (obb->GetWoaldPos())), &obb->GetDirect(dir)) / len;

		//sの値から1引くことで、obbの法線ベクトルが指している辺から球の中心までの距離を求められる
		//1以下の場合今見ている法線ベクトルが指している辺だけで見るとぶつかっていることになるのでshortestDistanceVecに値を足さない
		projectionLineRatio = fabs(projectionLineRatio);
		if (projectionLineRatio > 1)
		{
			shortestDistanceVec += (1 - projectionLineRatio) * len * obb->GetDirect(dir);
		}
	}

	//obbと球の最短距離が球の半径より長ければぶつかっていない
	if (D3DXVec3Length(&shortestDistanceVec) > sphere->Getradius())
	{
		//ぶつかってない
		return false;
	}

	//ぶつかってる
	return true;
}

//球体同士の衝突判定
//引数：circleA	１つ目の球体判定
//引数：circleB	２つ目の球体判定
//戻値：接触していればtrue
bool Collider::IsHitCircleVsCircle(SphereCollider* circleA, SphereCollider* circleB)
{
	D3DXVECTOR3 v = (circleA->worldCenter_) 
		- (circleB->worldCenter_);

	if (D3DXVec3Length(&v) <= (circleA->radius_ + circleB->radius_))
	{
		return true;
	}

	return false;
}

void Collider::PushBackObbFromWall(BoxCollider* obb, D3DXVECTOR3 wallPos, D3DXVECTOR3 wallNormal)
{
	//平面の法線に対するobbの射影線の長さを求める
	float projectionLineLen = 0;

	//// 各軸についてはみ出た部分のベクトルを算出
	for (int dir = DIR_X; dir < DIR_QUANTITY; dir++)
	{
		//内積からobbの各軸の壁の法線方向の長さを計算し足す
		projectionLineLen += fabs(D3DXVec3Dot(&(obb->GetDirect(dir) * obb->GetLen(dir)), &wallNormal));
	}

	//平面とobbの中心点の最短距離を調べる
	float shortestDistance = D3DXVec3Dot(&(obb->GetWoaldPos() - wallPos), &wallNormal);

	//obbを戻す
	D3DXVECTOR3 returnPos = obb->pGameObject_->GetPosition();
	//shortestDisがプラスのときはobbの中心は壁に埋まっていない
	//そのため戻す距離ははみ出た部分 - 最短距離になる
	if (shortestDistance > 0)
	{
		returnPos += wallNormal * (projectionLineLen - fabs(shortestDistance));
		obb->pGameObject_->SetPosition(returnPos);
	}
	//shortestDistanceがマイナスのときはobbが中心よりも壁にめり込んでいる
	//そのため戻す距離ははみ出し部分 + 最短距離になる
	else
	{
		returnPos += wallNormal * (projectionLineLen + fabs(shortestDistance));
		obb->pGameObject_->SetPosition(returnPos);
	}
}

//テスト表示用の枠を描画
//引数：position	位置
void Collider::Draw()
{
	D3DXMATRIX mat;

	D3DXMATRIX mX;
	D3DXMatrixRotationX(&mX, D3DXToRadian(worldRotate_.x));
	D3DXMATRIX mY;
	D3DXMatrixRotationY(&mY, D3DXToRadian(worldRotate_.y));
	D3DXMATRIX mZ;
	D3DXMatrixRotationZ(&mZ, D3DXToRadian(worldRotate_.z));

	D3DXMatrixTranslation(&mat, worldCenter_.x, worldCenter_.y, worldCenter_.z);
	mat = mX * mY * mZ  * mat;

	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &mat);
	pMesh_->DrawSubset(0);
}


