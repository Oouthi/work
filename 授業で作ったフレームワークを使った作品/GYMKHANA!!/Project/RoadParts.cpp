#include "RoadParts.h"

//コンストラクタ
RoadParts::RoadParts(IGameObject * parent)
	:RoadParts(parent, "RoadParts")
{
}

RoadParts::RoadParts(IGameObject * parent, std::string name):IGameObject(parent, name),
pEffect_(nullptr)
{
}

//デストラクタ
RoadParts::~RoadParts()
{
}

//初期化
void RoadParts::Initialize()
{
}

//更新
void RoadParts::Update()
{
}

//描画
void RoadParts::Draw()
{
}

//開放
void RoadParts::Release()
{
}