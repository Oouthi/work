#pragma once
#include "Engine/global.h"

class Map;
class Controller;

//プレイシーンを管理するクラス
class PlayScene : public IGameObject
{
	//外部からマップを参照する際に使うポインタ
	Map* pMap_;
	//外部からコントローラーを参照する際に使うポインタ
	Controller* pController_;

public:
  //コンストラクタ
  //引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

  //初期化
  void Initialize() override;

  //更新
  void Update() override;

  //描画
  void Draw() override;

  //開放
  void Release() override;

  //衝突
  void OnCollision(IGameObject *pTarget) override;

  //pMap_のゲッター
  Map* GetMap()
  {
	  return pMap_;
  }

  //pControllerのゲッター
  Controller* GetController()
  {
	  return pController_;
  }
};