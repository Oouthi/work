#include <string>
#include <Windows.h>
#include "Controller.h"
#include "Engine/Camera.h"
#include "Engine/Model.h"
#include "Engine/Image.h"
#include "Engine/Global.h"
#include "Map.h"
#include "PlayScene.h"

//移動速度
const float MOVESPEED = 0.25f;
//マウス感度
const float MOUSE_SENSITIVITY = 3.0f;
//画像ファイルのパス
const std::string senterPointPictPath = "data/Picture/CenterPoint.png";
const std::string selectBlockPictPath = "data/Picture/SelectBlock.png";
const std::string BlockPictPath[BLOCK_MAX]
{
	"Data/Picture/DefaultBlock.png",
	"Data/Picture/BrickBlock.png",
	"Data/Picture/GrassBlock.png",
	"Data/Picture/SandBlock.png",
	"Data/Picture/Water.png",
};

//コンストラクタ
Controller::Controller(IGameObject * parent)
	:IGameObject(parent, "Controller"), selectBlock_(BLOCK_BRICK), hSenterPointPict_(-1), hSelectBlockPict_(-1)
{
}

//デストラクタ
Controller::~Controller()
{
}

//初期化
void Controller::Initialize()
{
	//初期位置
	position_.z -= 10;

	//カメラの生成
	Camera *pCamera = CreateGameObject<Camera>(this);
	
	//カメラの設定
	pCamera->SetPosition(D3DXVECTOR3(0, 0, 0));
	pCamera->SetTarget(D3DXVECTOR3(0, 0, 10));

	//画面中心の目印画像のロード
	hSenterPointPict_ = Image::Load(senterPointPictPath);
	assert(hSenterPointPict_ >= 0);
	//SelectBlock文字画像のロード
	hSelectBlockPict_ = Image::Load(selectBlockPictPath);
	assert(hSelectBlockPict_ >= 0);
	//選んでいるブロックの文字画像のロード
	for (int i = 0; i < BLOCK_MAX; i++)
	{
		hBlockPict_[i] = -1;
		hBlockPict_[i] = Image::Load(BlockPictPath[i]);
		assert(hBlockPict_[i] >= 0);
	}
}

//更新
void Controller::Update()
{
	Move();

	//マウスを左クリックしたらブロックを置く
	if (Input::IsMouseButtonDown(0))
	{
		DecideBlockToAdd();
	}

	//マウスを右クリックしたらブロックを消す
	if (Input::IsMouseButtonDown(1))
	{
		DecideBlockToRemove();
	}

	//Rが押されたらマウスの表示 / 非表示を切り替える
	if (Input::IsKeyDown(DIK_R))
	{
		SwitchDrawCursor();
	}

	//アンドゥ
	if (Input::IsKeyDown(DIK_E))
	{
		Undo();
	}

	//リドゥ
	if (Input::IsKeyDown(DIK_Q))
	{
		Redo();
	}
}

//描画
void Controller::Draw()
{
	//画像の表示位置用
	D3DXMATRIX imageMatrix;
	D3DXMatrixIdentity(&imageMatrix);

	//画面中心の目印の表示
	D3DXMatrixTranslation(&imageMatrix, Global::GetWindowSize().width / 2 - Image::GetImageInfo(hSenterPointPict_).Width / 2, 
										Global::GetWindowSize().height / 2 - Image::GetImageInfo(hSenterPointPict_).Height / 2, 0);
	Image::SetMatrix(hSenterPointPict_, imageMatrix);
	Image::Draw(hSenterPointPict_);

	//SelectBlock文字画像の表示
	D3DXMatrixIdentity(&imageMatrix);
	Image::SetMatrix(hSelectBlockPict_, imageMatrix);
	Image::Draw(hSelectBlockPict_);

	//ブロックの種類文字画像の表示
	D3DXMatrixTranslation(&imageMatrix, Image::GetImageInfo(hSelectBlockPict_).Width, 0, 0);
	Image::SetMatrix(hBlockPict_[selectBlock_], imageMatrix);
	Image::Draw(hBlockPict_[selectBlock_]);
}

//開放
void Controller::Release()
{
}

void Controller::InitRedoStack()
{
	while (!redoStack.empty())
	{
		redoStack.pop();
	}
	while (!redoBlockStack.empty())
	{
		redoBlockStack.pop();
	}
}

void Controller::InitUndoStack()
{
	while (!undoStack.empty())
	{
		undoStack.pop();
	}
	while (!undoBlockStack.empty())
	{
		undoBlockStack.pop();
	}
}

void Controller::DecideBlockToAdd()
{
	RayCastData rayCastData;

	////レイキャストがモデルに当たったかどうかを入れる
	bool isHit = false;

	////マップのブロック全てと判定をするために
	////コントローラーの持ち主であるプレイシーンを通じてマップが持っているブロックの情報をもらう
	const std::vector<BlockInfo> blockVector = ((Map*)((PlayScene*)pParent_)->GetMap())->GetBlockVector();

	//一番距離が近いブロックの配列番号
	int minDistanseBlockNumber;

	//レイキャスト
	BlockRayCast(rayCastData, minDistanseBlockNumber, isHit);

	//ブロックに当たっていてかつ例の発射位置とブロックまでの距離がブロックのサイズより離れている場合ブロックを追加する
	if (isHit && rayCastData.hitDistance > BLOCK_SIZE)
	{
		BlockInfo blockInfo;
		blockInfo.type = selectBlock_;
		blockInfo.position = (blockVector)[minDistanseBlockNumber].position + rayCastData.normal;
		((Map*)((PlayScene*)pParent_)->GetMap())->AddBlock(blockInfo);
	
		//アンドゥスタックに追加
		undoStack.push(true);
	
		//リドゥスタックをクリア
		InitRedoStack();
	}
}

void Controller::DecideBlockToRemove()
{
	RayCastData rayCastData;

	//レイキャストがモデルに当たったかどうかを入れる
	bool isHit = false;

	//マップのブロック全てと判定をするために
	//コントローラーの持ち主であるプレイシーンを通じてマップが持っているブロックの情報をもらう
	const std::vector<BlockInfo> blockVector = ((Map*)((PlayScene*)pParent_)->GetMap())->GetBlockVector();

	//一番距離が近いブロックの配列番号
	int minDistanseBlockNumber;

	//レイキャスト
	BlockRayCast(rayCastData, minDistanseBlockNumber, isHit);

	//レイキャストが当たっていたらマップにブロックのブロックを削除する
	if (isHit)
	{
		//アンドゥスタックに溜める
		undoStack.push(false);

		//消すブロックの情報をスタックに溜める
		undoBlockStack.push(blockVector[minDistanseBlockNumber]);

		//ブロックを消す
		((Map*)((PlayScene*)pParent_)->GetMap())->RemoveBlock(minDistanseBlockNumber);


		//リドゥスタックをクリア
		while (!redoStack.empty())
		{
			redoStack.pop();
		}
		while (!redoBlockStack.empty())
		{
			redoBlockStack.pop();
		}
	}
}

void Controller::BlockRayCast(RayCastData & rayCastData, int & minDistanseBlockNumber, bool& isHit)
{
	//レイキャストデータの初期化
	//レイキャストを発射する方向
	const D3DXVECTOR3 front(0, 0, 1);
	D3DXMATRIX m, mX, mY, mZ;
	D3DXMatrixRotationX(&mX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&mY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&mZ, D3DXToRadian(rotate_.z));
	m = mX * mY * mZ;
	D3DXVec3TransformCoord(&rayCastData.direction, &front, &m);

	//レイキャストを発射する原点
	rayCastData.origin = position_;

	//当たったかのフラグを折っておく
	rayCastData.hit = false;

	//発射したレイキャストに当たった中で一番近いモデルまでの距離 初期値より遠い場合は判定しない事とする
	float minDistanse = 9999;

	//発射したレイキャストに当たった中で一番近い面の邦船
	D3DXVECTOR3 minDistanceNormal = D3DXVECTOR3(0, 0, 0);

	//ブロックに一つでも当たったかどうかのフラグ
	isHit = false;

	//マップのブロック全てと判定をするために
	//コントローラーの持ち主であるプレイシーンを通じてマップが持っているブロックの情報をもらう
	const std::vector<BlockInfo> blockVector = ((Map*)((PlayScene*)pParent_)->GetMap())->GetBlockVector();

	//全ブロックに対してレイキャストとの当たり判定をする
	for (int i = 0; i < blockVector.size(); i++)
	{
		//モデルはフライウェイトパターンを使って管理されているので
		//Modelネームスペースはモデル共有部分しかもっていない
		//そのため描画するときと同じように位置をセットしてからレイキャストをする
		D3DXMATRIX matrix;
		D3DXMatrixTranslation(&matrix, (blockVector)[i].position.x, (blockVector)[i].position.y, (blockVector)[i].position.z);

		Model::SetMatrix((blockVector)[i].type, matrix);

		Model::RayCast((blockVector)[i].type, rayCastData);

		//モデルがレイキャストに当たっていたか
		if (rayCastData.hit)
		{
			isHit = true;

			//minDistanseより今当たったブロックまでの距離が短い場合データを更新していく
			if (minDistanse > rayCastData.hitDistance)
			{
				minDistanse = rayCastData.hitDistance;
				minDistanceNormal = rayCastData.normal;
				//今調べている要素のイテレータをminDistanseModelItrに入れる
				minDistanseBlockNumber = i;
			}
		}
	}

	//当たった中で一番近いブロックまでの距離と法線を入れなおす
	rayCastData.hitDistance = minDistanse;
	rayCastData.normal = minDistanceNormal;
}

void Controller::Undo()
{
	//アンドゥ用スタックにコマンドがなければアンドゥできない
	if (undoStack.empty())
	{
		return;
	}

	if (undoStack.top())
	{
		//コントローラーの持ち主であるプレイシーンを通じてマップが持っているブロックの情報をもらう
		const std::vector<BlockInfo> blockVector = ((Map*)((PlayScene*)pParent_)->GetMap())->GetBlockVector();

		//リドゥ用スタックに追加
		redoStack.push(undoStack.top());

		//ブロックを消す前にリドゥで戻せるように最後に追加されたブロックをリドゥ用ブロックスタックに追加
		redoBlockStack.push(blockVector.back());

		//最後に行われた操作がブロックの追加なので最後に追加されたブロックを消す
		((Map*)((PlayScene*)pParent_)->GetMap())->RemoveBlock(blockVector.size() - 1);
	}
	else
	{
		//リドゥ用スタックに追加
		redoStack.push(undoStack.top());

		//最後に行われた操作がブロックの削除なので最後に消されたブロックを追加する
		((Map*)((PlayScene*)pParent_)->GetMap())->AddBlock(undoBlockStack.top());

		//消したブロックの情報を入れていたアンドゥ用ブロックスタックから消す
		undoBlockStack.pop();
	}

	//アンドゥしたのでアンドゥ用スタックからコマンドを消す
	undoStack.pop();
}

void Controller::Redo()
{
	//リドゥ用スタックにコマンドがなければリドゥできない
	if (redoStack.empty())
	{
		return;
	}

	if (redoStack.top())
	{
		//アンドゥで用スタックに追加
		undoStack.push(redoStack.top());

		//アンドゥで消されたブロックをリドゥ用ブロックスタックから戻す
		((Map*)((PlayScene*)pParent_)->GetMap())->AddBlock(redoBlockStack.top());

		//アンドゥで消したブロックの情報を入れていたリドゥ用ブロックスタックから消す
		redoBlockStack.pop();
	}
	else
	{
		//コントローラーの持ち主であるプレイシーンを通じてマップが持っているブロックの情報をもらう
		const std::vector<BlockInfo> blockVector = ((Map*)((PlayScene*)pParent_)->GetMap())->GetBlockVector();

		//リドゥ用スタックに追加
		undoStack.push(redoStack.top());

		//アンドゥで戻したブロックを消す前に再びアンドゥで戻せるようにアンドゥブロックスタックに追加する
		undoBlockStack.push(blockVector.back());

		//アンドゥで戻したブロックを消す
		((Map*)((PlayScene*)pParent_)->GetMap())->RemoveBlock(blockVector.size() - 1);
	}

	//リドゥしたのでリドゥ用スタックからコマンドを消す
	redoStack.pop();
}

void Controller::Move()
{
	//マウスの移動量に応じて視点を回転させる
	rotate_.y += Input::GetMouseMove().x / MOUSE_SENSITIVITY;
	rotate_.x += Input::GetMouseMove().y / MOUSE_SENSITIVITY;

	if (rotate_.x > 85)
	{
		rotate_.x = 85;
	}
	else if (rotate_.x < -85)
	{
		rotate_.x = -85;
	}

	//移動
	D3DXVECTOR3 move;
	//moveを変形させるために行列を作る
	D3DXMATRIX mY;
	D3DXMatrixRotationY(&mY, D3DXToRadian(rotate_.y));
	D3DXMATRIX mX;
	D3DXMatrixRotationX(&mX, D3DXToRadian(rotate_.x));
	D3DXMATRIX mZ;
	D3DXMatrixRotationZ(&mZ, D3DXToRadian(rotate_.z));
	D3DXMATRIX wm;
	wm = mX * mZ * mY;

	//移動前
	if (Input::IsKey(DIK_W))
	{
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, MOVESPEED), &mY);
		position_ += move;
	}
	//移動後ろ
	if (Input::IsKey(DIK_S))
	{
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(0, 0, -MOVESPEED), &mY);
		position_ += move;
	}
	//移動右
	if (Input::IsKey(DIK_D))
	{
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(MOVESPEED, 0, 0), &mY);
		position_ += move;
	}
	//移動左
	if (Input::IsKey(DIK_A))
	{
		D3DXVec3TransformCoord(&move, &D3DXVECTOR3(-MOVESPEED, 0, 0), &mY);
		position_ += move;
	}
	//移動上
	if (Input::IsKey(DIK_SPACE))
	{
		position_.y += MOVESPEED;
	}
	//移動下
	if (Input::IsKey(DIK_LSHIFT))
	{
		position_.y -= MOVESPEED;
	}
}

void Controller::SwitchDrawCursor()
{
	static bool isDrawCursor = ShowCursor(true);

	if (isDrawCursor)
	{
		while (ShowCursor(false) >= 0);
		isDrawCursor = false;
	}
	else
	{
		while (ShowCursor(true) < 0);
		isDrawCursor = true;
	}
}
