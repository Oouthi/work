#include "Map.h"
#include "Engine/Model.h"

const std::string BlockModelPath[BLOCK_MAX]
{
	"Data/Model/DefaultBlock.fbx",
	"Data/Model/BrickBlock.fbx",
	"Data/Model/GrassBlock.fbx",
	"Data/Model/SandBlock.fbx",
	"Data/Model/WaterBlock.fbx",
};

//コンストラクタ
Map::Map(IGameObject * parent)
	:IGameObject(parent, "Map")
{
}

//デストラクタ
Map::~Map()
{
}

//初期化
void Map::Initialize()
{
	//モデルデータのロード
	for (int i = 0; i < BLOCK_MAX; i++)
	{
		hBlockModel_[i] = -1;
		hBlockModel_[i] = Model::Load(BlockModelPath[i]);
		assert(hBlockModel_[i] >= 0);
	}
	//最初にブロックを一つだけ出しておく
	BlockInfo blockInfo;
	blockInfo.type = hBlockModel_[BLOCK_BRICK];
	blockInfo.position = D3DXVECTOR3(0, 0, 0);
	blockVector_.push_back(blockInfo);

}

//更新
void Map::Update()
{
}

//描画
void Map::Draw()
{
	//ブロックを表示
	for (int i = 0; i < blockVector_.size(); i++)
	{
		D3DXMATRIX matrix;
		D3DXMatrixTranslation(&matrix, blockVector_[i].position.x, blockVector_[i].position.y, blockVector_[i].position.z);
		Model::SetMatrix(hBlockModel_[blockVector_[i].type], matrix);
		Model::Draw(hBlockModel_[blockVector_[i].type]);
	}
}

//開放
void Map::Release()
{
}

void Map::AddBlock(BlockInfo blockInfo)
{
	blockVector_.push_back(blockInfo);
}

void Map::RemoveBlock(int number)
{
	//サイズが1の場合消すと新しくブロックを作れないので消さないようにする
	if (blockVector_.size() > 1)
	{
		blockVector_.erase(blockVector_.begin() + number);
	}
}

void Map::InitializeBlockVector()
{
	blockVector_.clear();
}

