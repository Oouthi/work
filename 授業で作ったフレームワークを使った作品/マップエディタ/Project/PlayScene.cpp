#include "PlayScene.h"
#include "Map.h"
#include "Controller.h"

//コンストラクタ
PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")	//親のコンストラクタを引数ありで呼ぶ
{
}

//初期化
void PlayScene::Initialize()
{
	//マップとコントローラーの生成
	pMap_ = CreateGameObject<Map>(this);
	pController_ = CreateGameObject<Controller>(this);
}

//更新
void PlayScene::Update()
{

}

//描画
void PlayScene::Draw()
{
}

//開放
void PlayScene::Release()
{
}

void PlayScene::OnCollision(IGameObject * pTarget)
{
}
