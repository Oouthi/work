#pragma once
#include "Global.h"

//スプライトとテクスチャを作らないと画像表示できない
//スプライトという板を用意して、テクスチャで色を付ける！
class Sprite
{
	LPD3DXSPRITE		pSprite_;	//スプライトは板
	LPDIRECT3DTEXTURE9	pTexture_;	//テクスチャは色

	//画像の情報 大きさの取得などに使う
	D3DXIMAGE_INFO imageInfo_;

public:
	Sprite();
	~Sprite();

	//スプライト・テクスチャを作成する
	//引数：FileName	画像入れ
	//戻値：なし
	void Load(const char* FileName);

	//行列を作成し描画する
	//引数：matrix	行列, カラー
	//戻値：なし
	void Draw(D3DXMATRIX& matrix, D3DXCOLOR color);	//描写処理

	//画像の情報を返す
	//戻値:D3DXIMAGE_INFO型 画像の情報
	D3DXIMAGE_INFO GetImageInfo()
	{
		return imageInfo_;
	}
};

