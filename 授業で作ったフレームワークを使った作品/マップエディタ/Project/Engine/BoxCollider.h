#pragma once
#include "Collider.h"
#include <string>

//方向ベクトルの数x,y,zで三つ
const int DIR_QUANTITY = 3;

enum NORMAL_DIR
{
	DIR_X = 0,
	DIR_Y,
	DIR_Z
};

//箱型の当たり判定
class BoxCollider :	public Collider
{
	friend class Collider;

	//判定サイズ（幅、高さ、奥行き
	D3DXVECTOR3 size_;
	// 方向ベクトル
	//回転するとここの値が変わる
	D3DXVECTOR3 m_NormaDirect_[DIR_QUANTITY];
	// 各軸方向の長さ
	float m_fLength[DIR_QUANTITY];

public:
	//コンストラクタ（当たり判定の作成）
	//引数：basePos	当たり判定の中心位置（ゲームオブジェクトの原点から見た位置）
	//引数：size	当たり判定のサイズ（幅、高さ、奥行き）
	BoxCollider(const std::string& name, D3DXVECTOR3 basePos, D3DXVECTOR3 size, IGameObject* pGameObject);
	BoxCollider(D3DXVECTOR3 basePos, D3DXVECTOR3 size , IGameObject* pGameObject);

	//x,y,zのベクトルを返す
	D3DXVECTOR3 GetDirect(int dir)
	{
		return m_NormaDirect_[dir];
	}

	//x,y,zの長さを返す
	float GetLen(int dir)
	{
		return m_fLength[dir];
	}

	void SetRotate(D3DXVECTOR3 rotate)
	{
		loaclRotate_ = rotate;
	}

	void WorldRotateAndPosCalc() override;

private:
	//接触判定
	//引数：target	相手の当たり判定
	//戻値：接触してればtrue
	bool IsHit(Collider* target) override;

};

