#include "Global.h"

namespace Global
{
	//ウィンドウのサイズ namespaceのためメンバ変数ではないが関数の引数名と被らないようにアンダースコアをつける
	WindowSize windowSize_;

	void SetWindowSize(WindowSize windowSize)
	{
		windowSize_ = windowSize;
	}
	WindowSize GetWindowSize()
	{
		return windowSize_;
	}
}