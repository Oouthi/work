#include "Camera.h"
#include "Direct3D.h"

//コンストラクタ
Camera::Camera(IGameObject * parent)
	:IGameObject(parent, "Camera"), target_(0, 0, 0)
{
}

//デストラクタ
Camera::~Camera()
{
}

//初期化
void Camera::Initialize()
{
	D3DXMATRIX proj; //プロジェクション行列を作るための変数
	//プロジェクション行列を作るための行列 (行列を保存するための変数,画角,アスペクト比ウィンドウの比率,描画し始める位置を面にしないといけないため少し離す,どこまで表示するか)
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60), (float)Global::GetWindowSize().width / Global::GetWindowSize().height, 0.5f, 1000.0f);
	//どの座標として使うかを指定
	Direct3D::pDevice->SetTransform(D3DTS_PROJECTION, &proj);
}

//更新
void Camera::Update()
{
	Transform();

	//ビュー行列
	D3DXVECTOR3 worldPosition, worldTarget;
	D3DXVec3TransformCoord(&worldPosition, &position_, &worldMatrix_);
	D3DXVec3TransformCoord(&worldTarget, &target_, &worldMatrix_);

	
	D3DXMATRIX view;
	D3DXMATRIX m, mX, mY, mZ;
	D3DXMatrixRotationX(&mX, D3DXToRadian(pParent_->GetRotate().x));
	D3DXMatrixRotationY(&mY, D3DXToRadian(pParent_->GetRotate().y));
	D3DXMatrixRotationZ(&mZ, D3DXToRadian(pParent_->GetRotate().z));
	m = mX * mY * mZ;
	D3DXVECTOR3 pUp = D3DXVECTOR3(0, 1, 0);
	D3DXVec3TransformCoord(&pUp, &pUp, &m);
	//ビュー行列を作るための関数 (カメラの情報を保存する変数, &変数に入れるカメラの位置,どの「位置」見るか,カメラそのものを傾ける
	D3DXMatrixLookAtLH(&view, &worldPosition, &worldTarget, &pUp);
	//どの座標として使うかを指定
	Direct3D::pDevice->SetTransform(D3DTS_VIEW, &view);
}

//描画
void Camera::Draw()
{
}

//開放
void Camera::Release()
{
}

