#include "IGameObject.h"
#include "Global.h"
#include "Collider.h"
#include "SceneManager.h"
#include "Direct3D.h"

IGameObject::IGameObject() :
	IGameObject(nullptr, "")
{
		colliderList_.clear();
}

IGameObject::IGameObject(IGameObject * parent) :
	IGameObject(parent, "")
{
}

IGameObject::IGameObject(IGameObject * parent, const std::string & name) :
	pParent_(parent), name_(name), position_(D3DXVECTOR3(0, 0, 0)),
	rotate_(D3DXVECTOR3(0, 0, 0)), scale_(D3DXVECTOR3(1, 1, 1)), dead_(false), isUpdate_(true)
{
	D3DXMatrixIdentity(&localMatrix_);
	D3DXMatrixIdentity(&worldMatrix_);
}

IGameObject::~IGameObject()
{
	for (auto it = colliderList_.begin(); it != colliderList_.end(); it++)
	{
		SAFE_DELETE(*it);
	}
	colliderList_.clear();
}

void IGameObject::Transform()
{
	D3DXMATRIX mX;
	D3DXMatrixRotationX(&mX, D3DXToRadian(rotate_.x));
	D3DXMATRIX mY;
	D3DXMatrixRotationY(&mY, D3DXToRadian(rotate_.y));
	D3DXMATRIX mZ;
	D3DXMatrixRotationZ(&mZ, D3DXToRadian(rotate_.z));

	D3DXMATRIX m;
	D3DXMatrixScaling(&m, scale_.x, scale_.y, scale_.z);

	D3DXMATRIX mT;
	D3DXMatrixTranslation(&mT, position_.x, position_.y, position_.z);

	D3DXMATRIX mW;
	//縮小・拡大は原点からの頂点の長さを半分にすることで行っているため移動の前に掛けると中心に寄っていってしまう
	//縮小・拡大する前に回転すると横面が正面に来た時に縮小・拡大が掛かって思った通りに行かないかも
	localMatrix_ = m * mX * mY * mZ * mT;

	if (pParent_ == nullptr)
	{
		worldMatrix_ = localMatrix_;
	}
	else
	{
		worldMatrix_ = localMatrix_ * pParent_->worldMatrix_;
	}
}

void IGameObject::PushBackChild(IGameObject* pObj)
{
	childList_.push_back(pObj);
}

void IGameObject::UpdateSub()
{
	if (isUpdate_)
	{
		Update();
	}
	Transform();

	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		(*i)->InisializeHitCollider();
	}

	Collision(SceneManager::GetCurrentScene());
	Transform();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->UpdateSub();
	}

	for (auto it = childList_.begin(); it != childList_.end();)
	{
		if ((*it)->dead_ == true)
		{
			(*it)->ReleaseSub();
			SAFE_DELETE(*it);
			it = childList_.erase(it);
		}
		else 
		{
			it++;
		}
	}
}

void IGameObject::DrawSub()
{
	Draw();

#ifdef _DEBUG
	CollisionDraw();
#endif

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->DrawSub();
	}
}

void IGameObject::ReleaseSub()
{
	Release();

	for (auto it = childList_.begin(); it != childList_.end(); it++)
	{
		(*it)->ReleaseSub();
		SAFE_DELETE(*it);
	}
}

void IGameObject::KillMe()
{
	//子供を殺すのは親だしupdataの途中で死なれると困るのでここではフラグを立てるだけ
	dead_ = true;
}

//コライダー（衝突判定）を追加する
void IGameObject::AddCollider(Collider* collider)
{
	collider->SetGameObject(this);
	colliderList_.push_back(collider);
}


//衝突判定
void IGameObject::Collision(IGameObject * pTarget)
{
	//自分同士の当たり判定はしない
	if (this == pTarget)
	{
		return;
	}

	//自分とpTargetのコリジョン情報を使って当たり判定
	//1つのオブジェクトが複数のコリジョン情報を持ってる場合もあるので二重ループ
	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		//あたり判定をする前にコライダーの位置を直しておく
		(*i)->WorldRotateAndPosCalc();

		for (auto j = pTarget->colliderList_.begin(); j != pTarget->colliderList_.end(); j++)
		{
			//相手のコライダーの位置も直す
			(*j)->WorldRotateAndPosCalc();

			if ((*i)->IsHit(*j))
			{
				(*i)->SetHitCollider((*j));
				//当たった
				this->OnCollision(pTarget);
			}
		}
	}

	//子供がいないなら終わり
	if (pTarget->childList_.empty())
		return;

	//子供も当たり判定
	for (auto i = pTarget->childList_.begin(); i != pTarget->childList_.end(); i++)
	{
		Collision(*i);
	}
}

//オブジェクトの名前を取得
std::string IGameObject::GetObjectName()
{
	return name_;
}

IGameObject * IGameObject::FindChildObject(const std::string & name)
{
	//子供がいないなら終わり
	if (childList_.empty())
		return nullptr;

	//イテレータ
	auto it = childList_.begin();	//先頭
	auto end = childList_.end();	//末尾

	//子オブジェクトから探す
	while (it != end) {
		//同じ名前のオブジェクトを見つけたらそれを返す
		if ((*it)->GetObjectName() == name)
			return *it;

		//その子供（孫）以降にいないか探す
		IGameObject* obj = (*it)->FindChildObject(name);
		if (obj != nullptr)
		{
			return obj;
		}

		//次の子へ
		it++;
	}

	//見つからなかった
	return nullptr;
}

//テスト用の衝突判定枠を表示
void IGameObject::CollisionDraw()
{
	Direct3D::pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);	//ワイヤーフレーム
	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);				//ライティングOFF
	Direct3D::pDevice->SetTexture(0, nullptr);								//テクスチャなし

	for (auto i = this->colliderList_.begin(); i != this->colliderList_.end(); i++)
	{
		(*i)->Draw();
	}

	Direct3D::pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	Direct3D::pDevice->SetRenderState(D3DRS_LIGHTING,TRUE);
}
