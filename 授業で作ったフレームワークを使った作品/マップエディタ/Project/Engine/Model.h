#pragma once
#include <string>
#include <vector>
#include "Fbx.h"

namespace Model
{
	struct ModelData
	{
		std::string fileName;
		Fbx* pFbx;
		D3DXMATRIX matrix;

		ModelData() : fileName(""), pFbx(nullptr)
		{
			D3DXMatrixIdentity(&matrix);
		}
	};

	int Load(std::string fileName, LPD3DXEFFECT pEffect = nullptr);

	void Draw(int handle);

	void SetMatrix(int handle, D3DXMATRIX& matirx);

	void Release(int handle);

	void AllRelease();

	//シェーダを設定する
	//引数:データ番号,シェーダの情報が入った変数
	void SetEffect(int handle, LPD3DXEFFECT pEffect);

	//レイキャストによる当たり判定
	//引数:データ番号,当たり判定の結果を入れる変数
	void RayCast(int handle, RayCastData &data);

	//モデルのデータリストを返す
	//戻り値:モデルのデータリスト
	std::vector<ModelData*> GetModelDataList();
}