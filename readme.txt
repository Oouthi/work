=====================================================================================
東北電子専門学校 ゲームエンジニア科 3年 大内隆司
=====================================================================================

作品としてタイムアタックゲーム、マップエディタ、パーティクルシステムを提出させていただきます。
タイムアタックゲーム、マップエディタ、パーティクルシステムの順で開発しました。
タイムアタックゲームとマップエディタは「授業で作ったフレームワークを使った作品」の中にあります。
提出させていただいた作品はすべてdirectX9を使用して作成しているため起動にはdirectX9のインストールが必要です。

ハードウェア仕様
	マシン名 : HP Envy 750-170jp
	ＣＰＵ : Intel(R) Core(TM) i7-6700 CPU @ 3.40GHz
	メモリ : 16.0GB RAM
	グラフィックボード : NVIDIA GeForce GTX 980 Ti

ソフトウェア仕様、使用しているSDK
	OS : Windows10
	統合開発環境 : Visual Studio2017
	開発言語 : C++
	SDK : DirectX SDK, FBX SDK